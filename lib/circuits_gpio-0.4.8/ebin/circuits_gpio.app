{application,circuits_gpio,
             [{applications,[kernel,stdlib,elixir]},
              {description,"Use GPIOs in Elixir"},
              {modules,['Elixir.Circuits.GPIO','Elixir.Circuits.GPIO.Nif',
                        circuits_gpio]},
              {registered,[]},
              {vsn,"0.4.8"}]}.
