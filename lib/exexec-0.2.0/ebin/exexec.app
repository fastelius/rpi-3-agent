{application,exexec,
             [{applications,[kernel,stdlib,elixir,logger,erlexec]},
              {description,"An idiomatic Elixir wrapper for erlexec."},
              {modules,['Elixir.Exexec','Elixir.Exexec.Extras',
                        'Elixir.Exexec.StreamOutput',
                        'Elixir.Exexec.StreamOutput.Server',
                        'Elixir.Exexec.ToErl']},
              {registered,[]},
              {vsn,"0.2.0"}]}.
